import {
  Component,
  OnInit,
} from '@angular/core';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`ChildDetail` component loaded asynchronously');

@Component({
  selector: 'child-detail',
  template: `<div dir="rtl">
    <p>برای پیاده‌سازی این سایت از فریمورک انگولار۲ و زبان های  </p>
        <p>html, css, typeScript </p>
        <p>استفاده شده‌است و از منبع زیر نیز  کمک گرفته‌ام</p>
        <a href="https://github.com/AngularClass/angular-starter">https://github.com/AngularClass/angular-starter</a>
    </div> 
`,
})
export class ChildDetailComponent implements OnInit {

  public ngOnInit() {
    console.log('hello `ChildDetail` component');
  }

}

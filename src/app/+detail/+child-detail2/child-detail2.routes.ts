import { ChildDetail2Component } from './child-detail2.component';

export const routes = [
  { path: '', component: ChildDetail2Component,  pathMatch: 'full' },
];

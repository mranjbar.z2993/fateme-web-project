import {
  Component,
  OnInit,
} from '@angular/core';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`Detail` component loaded asynchronously');

@Component({
  selector: 'detail',
  template: `
    <div dir="rtl">
    <h1 >جزئیات </h1>
    <span>
      <a [routerLink]=" ['./child-detail'] ">
        زبان‌های مورد استفاده در سمت کلاینت
      </a><br><br>
      <a [routerLink]=" ['./child-detail2'] ">
       پایگاه داده
      </a><br><br>
      <a [routerLink]=" ['./child-detail3'] ">
     پروژه درس طراحی صفحات وب
      </a><br><br>
      <a [routerLink]=" ['./child-detail4'] ">
       
      </a><br><br>
    </span>
    <router-outlet></router-outlet>
    <div>
  `,
})
export class DetailComponent implements OnInit {

  public ngOnInit() {
    console.log('hello `Detail` component');
  }

}

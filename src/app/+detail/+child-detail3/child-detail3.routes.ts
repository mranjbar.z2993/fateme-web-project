import { ChildDetail3Component } from './child-detail3.component';

export const routes = [
  { path: '', component: ChildDetail3Component,  pathMatch: 'full' },
];

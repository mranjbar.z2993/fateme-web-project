import {
  Component,
  OnInit,
} from '@angular/core';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`ChildDetail3` component loaded asynchronously');

@Component({
  selector: 'child-detail3',
  template: `
<div dir="rtl">
<br><br><br>
    <p>درس طراحی صفحات وب در نیمسال دوم ۹۵-۹۶ در دانشکده‌ی شریعتی  توسط استاد موسی‌زاده ارائه شد، و این سایت برای پروژه‌ی این درس میباشد</p>    
    </div>
  `,
})
export class ChildDetail3Component implements OnInit {

  public ngOnInit() {
    console.log('hello `ChildDetail3` component');
  }

}

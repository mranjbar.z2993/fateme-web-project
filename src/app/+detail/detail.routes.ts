import { DetailComponent } from './detail.component';

export const routes = [
  { path: '', children: [
    { path: '', component: DetailComponent },
    { path: 'child-detail', loadChildren: './+child-detail#ChildDetailModule' },
    { path: 'child-detail2', loadChildren: './+child-detail2#ChildDetail2Module' },
    { path: 'child-detail3', loadChildren: './+child-detail3#ChildDetail3Module' },
    { path: 'child-detail4', loadChildren: './+child-detail4#ChildDetail4Module' }
  ]},
];

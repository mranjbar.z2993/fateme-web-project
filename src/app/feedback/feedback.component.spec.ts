import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

/**
 * Load the implementations that should be tested.
 */
import { FeedbackComponent } from './feedback.component';

describe('Feedback', () => {
  /**
   * Provide our implementations or mocks to the dependency injector
   */
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      /**
       * Provide a better mock.
       */
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      FeedbackComponent
    ]
  }));

  it('should log ngOnInit', inject([FeedbackComponent], (feedback: FeedbackComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    feedback.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
